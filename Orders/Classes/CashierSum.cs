﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orders
{
    [ProtoBuf.ProtoContract]
    public class CashierSum
    {

        [ProtoBuf.ProtoMember(1)]
        public decimal Sum { get; set; }
        [ProtoBuf.ProtoMember(2)]
        public DateTime date { get; set; }
        [ProtoBuf.ProtoMember(3)]
        public List<TransactionData> Transactions { get; set; }

        public CashierSum()
        {
            date = DateTime.Today;
        }

        public CashierSum(decimal value)
        {
            Sum = value;
            date = DateTime.Today;
        }
    }
}
