﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Orders
{
    [Table(Name = "APOTIKH")]
    public class DatabaseProduct
    {
        [Column(Name = "AP_DESCRIPTION", DbType = "VarChar(60)")]
        public string Description;

        [Column(Name = "AP_ID", DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true)]
        public int ID;

        [Column(Name = "AP_MORFI", DbType = "VarChar(80)")]
        public string Morfi;

        [Column(Name = "AP_TIMH_XON", DbType = "Money")]
        public decimal TimiXondrikis;

        [Column(Name = "AP_CODE", DbType = "VarChar(12)")]
        public string KodikosApothikis;

        [Column(Name = "AP_CODE_EOF", DbType = "VarChar(13)")]
        public string KodikosEOF;

        private EntitySet<ProductBarcode> _Barcodes;
        [Association(Storage = nameof(_Barcodes), OtherKey = nameof(ProductBarcode.Apothiki_ID))]
        public EntitySet<ProductBarcode> BarcodeS
        {
            get { return this._Barcodes; }
            set { this._Barcodes.Assign(value); }
        }

        public string FullName { get { return Description + " " + Morfi; } }

        public DatabaseProduct()
        {
            this._Barcodes = new EntitySet<ProductBarcode>();
        }
    }
}
