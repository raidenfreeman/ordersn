﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Orders
{
    [ProtoContract]
    public class TransactionData
    {
        [ProtoMember(1)]
        public string CustomerCode { get; set; }
        [ProtoMember(2)]
        public string Descritpion { get; set; }
        [ProtoMember(3)]
        public decimal Sum { get; set; }
        [ProtoMember(4)]
        public DateTime DateTime { get; set; }

        public TransactionData() { }
    }
}
