﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orders
{
    public partial class NewChangeDB : Form
    {
        public NewChangeDB()
        {
            InitializeComponent();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Properties.Settings.Default.DatabaseCatalogName = "Farnet_" + textBox1.Text;
                Properties.Settings.Default.Save();
                MessageBox.Show("Η βάση άλλαξε στην: " + "Farnet_" + textBox1.Text);
                this.Close();
            }
        }
    }
}
