﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Orders
{
    public partial class MainForm : Form
    {
        public const string MainSaveFile = "orderData.bin";
        public const string PrintedDir = "Printed";
        public const string BackupFile1 = "~orderData.bin";
        public const string BackupFile2 = "~orderData2.bin";
        public const string MainCashierSaveFile = "cashierData.bin";
        public const int EAN13length = 13;

        public static decimal tameio = 0;

        public List<OrderedProduct> orderList;
        public List<OrderedProduct> printedList;
        public List<OrderedProduct> displayedList;
        public List<CashierSum> cashierSumList;
        public decimal displayedListTotal;
        public int displayedListTotalItems;

        private List<string> barcodesToLookupQueue;
        public MainForm()
        {
            InitializeComponent();
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            this.Text = Text + " " + version.Major + "." + version.Minor + " (build " + version.Build + ")";
            orderList = new List<OrderedProduct>();
            printedList = new List<OrderedProduct>();
            displayedList = new List<OrderedProduct>();
            cashierSumList = new List<CashierSum>();
            barcodesToLookupQueue = new List<string>();
            this.Size = Properties.Settings.Default.WindowSize;
            this.Location = Properties.Settings.Default.WindowLocation;
            this.WindowState = Properties.Settings.Default.WindowState;

        }

        private void αλλαγήΒάσηςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var changeDBDialog = new NewChangeDB();
            changeDBDialog.ShowDialog();
        }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        private void backgroundWorkerBarcodeFinder_DoWork(object sender, DoWorkEventArgs e)
        {
            string barcodeToLookup = e.Argument.ToString();

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            //Table<DatabaseProduct> Prod = db.GetTable<DatabaseProduct>().Where(x=>x.BarcodeS.Any(y=>y.Barcode == barcodeToLookup)).FirstOrDefault();
            //var q = from c in Prod where c.BarcodeS.Any(x => x.Barcode == barcodeToLookup) select c;
            //var result = q.FirstOrDefault();
            DatabaseProduct result = null;
            try { result = db.GetTable<DatabaseProduct>().Where(x => x.BarcodeS.Any(y => y.Barcode == barcodeToLookup)).FirstOrDefault(); }
            catch (Exception)
            { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

            if (result != null)
            {
                var foundProduct = new OrderedProduct();
                foundProduct.AdditionTime = DateTime.Now;
                foundProduct.Name = result.Description + " " + result.Morfi;
                foundProduct.Barcode = barcodeToLookup;//result.BarcodeS.FirstOrDefault().Barcode;
                foundProduct.Price = result.TimiXondrikis;
                foundProduct.Code = result.KodikosApothikis;
                foundProduct.HasEOFCode = result.KodikosEOF != null && result.KodikosEOF != string.Empty;
                e.Result = foundProduct;
            }
            else
            {
                //look for a warehouse code
                result = db.GetTable<DatabaseProduct>().Where(x => x.KodikosApothikis == barcodeToLookup).FirstOrDefault();
                if (result != null)
                {
                    var foundProduct = new OrderedProduct();
                    foundProduct.AdditionTime = DateTime.Now;
                    foundProduct.Code = result.KodikosApothikis;
                    foundProduct.Name = result.Description + " " + result.Morfi;
                    foundProduct.HasEOFCode = result.KodikosEOF != null && result.KodikosEOF != string.Empty;
                    var resBarcds = result.BarcodeS.FirstOrDefault();
                    if (resBarcds != null)
                        foundProduct.Barcode = resBarcds.Barcode;
                    else
                    {
                        foundProduct.Barcode = "0000000000000";
                        //MessageBox.Show("ΠΡΟΣΟΧΉ: Το είδος δεν έχει barcode");
                    }
                    foundProduct.Price = result.TimiXondrikis;
                    e.Result = foundProduct;
                }
                else
                    e.Result = null;
            }
        }

        private void backgroundWorkerBarcodeFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("To είδος δεν βρέθηκε.");
                comboBox1.SelectAll();
                return;
            }
            AddFoundProduct((OrderedProduct)e.Result);
            comboBox1.Text = "";
            comboBox1.Focus();
        }

        private void AddFoundProduct(OrderedProduct tempProd)
        {
            tabControl1.SelectedIndex = tempProd.HasEOFCode ? 0 : 1;
            var productInOrder = orderList.Find(x => x.Code == tempProd.Code);
            if (productInOrder != null)
            {
                productInOrder.AdditionTime = DateTime.Now;
                orderList = orderList.OrderBy(x => x.Code == productInOrder.Code).ToList();
            }
            else
            {
                orderList.Add(tempProd);
            }
            StartSaveThread();
            UpdateDataGrid(orderList);
            comboBox1.SelectedIndex = -1;// .Items.Clear(); comboBox1.Text = "";
            if (backgroundWorkerBarcodeFinder.IsBusy == false)
            {
                string rest = barcodesToLookupQueue.FirstOrDefault();
                if (rest != null)
                {
                    barcodesToLookupQueue.RemoveAt(0);
                    backgroundWorkerBarcodeFinder.RunWorkerAsync(rest);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*List<OrderedProduct> listToDisplay = new List<OrderedProduct>();
            DateTime from = dateTimePicker1.Value;
            DateTime to = dateTimePicker2.Value;
            if (from != to)
            {
                foreach (var orderedProd in orderList)
                {
                    var additionTimes = orderedProd.AdditionTimes.Where(x => x >= from && x <= to);
                    int temaxia = additionTimes.Count();
                    if (temaxia > 0)
                    {
                        var tempProduct = new OrderedProduct();
                        tempProduct.Barcode = orderedProd.Barcode;
                        tempProduct.Name = orderedProd.Name;
                        foreach (var additionTime in additionTimes)
                        {
                            tempProduct.AdditionTime = additionTime;
                        }
                        listToDisplay.Add(tempProduct);
                    }
                }
            }
            else
                listToDisplay = orderList;
            UpdateDataGrid(listToDisplay);*/
            UpdateDataGrid(orderList);
        }

        private void UpdateDataGrid(IEnumerable<OrderedProduct> list)
        {
            dataGridView1.Columns["Delete"].Visible = true;
            dataGridView2.Columns["dataGridViewCheckBoxColumn1"].Visible = true;
            displayedListTotalItems = 0;
            displayedListTotal = 0;
            this.Invoke((MethodInvoker)delegate
            {
                foreach (var p in list.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)))
                {
                    displayedListTotal += p.Price * p.Quantity;
                    displayedListTotalItems += p.Quantity;
                }
                displayedList = list.ToList();
                if (displayedList.Count != 0)
                {
                    displayedList.Reverse();
                    var sortableList = new SortableBindingList<OrderedProduct>(displayedList);

                    var farmakaList = sortableList.Where(x => x.HasEOFCode == true);
                    var parafarmakaList = sortableList.Where(x => x.HasEOFCode == false);

                    var farmakaSource = new BindingSource(farmakaList, null);
                    var parafarmakaSource = new BindingSource(parafarmakaList, null);

                    dataGridView1.DataSource = farmakaSource;
                    if (dataGridView1.CurrentCell != null)
                        dataGridView1.CurrentCell.Selected = false;
                    dataGridView2.DataSource = parafarmakaSource;
                    if (dataGridView2.CurrentCell != null)
                        dataGridView2.CurrentCell.Selected = false;
                }
                else
                {
                    dataGridView1.DataSource = null;
                    dataGridView2.DataSource = null;
                }
                textBox2.Text = displayedListTotal.ToString("0.00") + " \u20AC";
                textBoxEidi.Text = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).Count().ToString();
                textBoxTemaxia.Text = displayedListTotalItems.ToString();
            });
        }

        void UpdateTextBoxTotals()
        {
            displayedListTotalItems = 0;
            displayedListTotal = 0;
            foreach (var p in displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)))
            {
                displayedListTotal += p.Price * p.Quantity;
                displayedListTotalItems += p.Quantity;
            }
            textBox2.Text = displayedListTotal.ToString("0.00") + " \u20AC";
            textBoxEidi.Text = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).Count().ToString();
            textBoxTemaxia.Text = displayedListTotalItems.ToString();
        }

        private void LoadListFromSaves()
        {
            /*if (Properties.Settings.Default.LastSavedData.Date != DateTime.Today)
                return;*/
            string saveFile;
            if (File.Exists(MainSaveFile))
                saveFile = MainSaveFile;
            else
            {
                if (File.Exists(BackupFile1))
                    saveFile = BackupFile1;
                else if (File.Exists(BackupFile2))
                    saveFile = BackupFile2;
                else
                {
                    //MessageBox.Show("Δεν βρέθηκαν σημερινά αρχεία των δεδομένων.");
                    return;
                }
            }
            using (var file = File.OpenRead(saveFile))
            {
                orderList = ProtoBuf.Serializer.Deserialize<List<OrderedProduct>>(file);
            }
            if (orderList.Count > 0)
                UpdateDataGrid(orderList);
        }

        private void LoadSumFromSaves()
        {
            /*if (Properties.Settings.Default.LastSavedData.Date != DateTime.Today)
                return;*/
            string saveFile;
            if (File.Exists(MainCashierSaveFile))
                saveFile = MainCashierSaveFile;
            else
                return;
            using (var file = File.OpenRead(saveFile))
            {
                cashierSumList = ProtoBuf.Serializer.Deserialize<List<CashierSum>>(file);
            }
            var result = cashierSumList.Where(x => x.date == DateTime.Today).FirstOrDefault();
            if (result != null)
            {
                tameio = result.Sum;
                this.Invoke((MethodInvoker)delegate
                {
                    textBoxResultMoney.Text = tameio.ToString("C");
                });
            }
        }

        private void SaveToFile()
        {
            Properties.Settings.Default.Save();
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(MainSaveFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var targetFile = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, orderList);
                }
            }
            catch (IOException)
            {
                Thread.Sleep(10);
                mantissa += "exception";
                using (var targetFile = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, orderList);
                }
            }
            try
            {
                File.Delete(MainSaveFile);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
                File.Delete(MainSaveFile + mantissa);
            }
            catch (IOException)
            {
                Thread.Sleep(20);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
            }
        }

        private void SaveSum()
        {
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(MainCashierSaveFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var targetFile = File.Create(MainCashierSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<CashierSum>>(targetFile, cashierSumList);
                }
            }
            catch (IOException)
            {
                Thread.Sleep(10);
                mantissa += "exception";
                using (var targetFile = File.Create(MainCashierSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<CashierSum>>(targetFile, cashierSumList);
                }
            }
            try
            {
                File.Delete(MainCashierSaveFile);
                File.Copy(MainCashierSaveFile + mantissa, MainCashierSaveFile);
                File.Delete(MainCashierSaveFile + mantissa);
            }
            catch (IOException)
            {
                Thread.Sleep(20);
                File.Copy(MainCashierSaveFile + mantissa, MainCashierSaveFile);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            backgroundWorkerLoad.RunWorkerAsync();
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Properties.Settings.Default.FarmakaDVGAltColor;
            dataGridView1.RowsDefaultCellStyle.BackColor = Properties.Settings.Default.FarmakaDVGMainColor;

            dataGridView2.AlternatingRowsDefaultCellStyle.BackColor = Properties.Settings.Default.ParafarmakaDVGAltColor;
            dataGridView2.RowsDefaultCellStyle.BackColor = Properties.Settings.Default.ParafarmakaDVGMainColor;

            textBox2.BackColor = Properties.Settings.Default.FarmakaSumsColor;
            textBoxEidi.BackColor = Properties.Settings.Default.FarmakaSumsColor;
            textBoxTemaxia.BackColor = Properties.Settings.Default.FarmakaSumsColor;

            textBoxResultMoney.BackColor = Properties.Settings.Default.CashierColor;
            textBoxMoney.BackColor = Properties.Settings.Default.CashierInputColor;
            comboBox1.BackColor = Properties.Settings.Default.BarcodeColor;

            if (Properties.Settings.Default.Installed == false)
            {
                InstallationForm n = new InstallationForm();
                n.ShowDialog();
            }

            #region getting all products

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            List<DatabaseProduct> result = new List<DatabaseProduct>();
            try { result = db.GetTable<DatabaseProduct>().OrderBy(x => x.Description).ToList(); }
            catch (Exception)
            { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }
            comboBox1.DataSource = result;
            comboBox1.DisplayMember = "FullName";
            comboBox1.SelectedIndex = -1;

            #endregion

        }

        private void backgroundWorkerSave_DoWork(object sender, DoWorkEventArgs e)
        {
            SaveToFile();
        }

        private void backgroundWorkerLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadListFromSaves();
            LoadSumFromSaves();
        }

        private void εκτύπωσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (displayedList.Count == 0)
                return;
            var listToPrint = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).ToList();
            PrintingManager p = new PrintingManager(listToPrint.OrderBy(x => x.Name).ToList());
            if (p.PrintAll())
            {
                printedList = listToPrint;
                RemovePrintedItems();
                displayedList = displayedList.Except(printedList).ToList();
                UpdateDataGrid(displayedList);
            }

        }

        private void RemovePrintedItems()
        {
            orderList = orderList.Except(printedList).ToList();
            StartSaveThread();
            if (dataGridView1.Columns["Delete"].Visible == true)
            {
                if (!Directory.Exists(PrintedDir))
                    Directory.CreateDirectory(PrintedDir);
                string date = DateTime.Today.ToString("dd_MM_yyyy_");
                string mantissa = "temp";
                string savefile = PrintedDir + '\\' + date + MainSaveFile;
                int i = 0;
                while (true)
                {
                    if (File.Exists(savefile))
                    {
                        i++;
                        savefile = PrintedDir + '\\' + date + MainSaveFile + '(' + (i + 1).ToString() + ')';
                    }
                    else
                        break;
                }
                using (var targetFile = File.Create(savefile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, printedList);
                }
                File.Delete(savefile);
                File.Copy(savefile + mantissa, savefile);
                File.Delete(savefile + mantissa);
            }
        }

        private void αποθήκευσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartSaveThread();
        }

        private void φόρτωσηToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            backgroundWorkerLoad.RunWorkerAsync();
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                displayedListTotal = 0;
                displayedListTotalItems = 0;
                foreach (var p in orderList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)))
                {
                    displayedListTotal += p.Price * p.Quantity;
                    displayedListTotalItems += p.Quantity;
                }
                textBox2.Text = displayedListTotal.ToString("0.00") + " \u20AC";
                textBoxEidi.Text = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).Count().ToString();
                textBoxTemaxia.Text = displayedListTotalItems.ToString();
                StartSaveThread();
            }
        }

        private void εκτύπωσηΣεΠροεπιλογήToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (displayedList.Count == 0)
                return;
            var listToPrint = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).ToList();
            PrintingManager p = new PrintingManager(listToPrint.OrderBy(x => x.Name).ToList());
            if (p.PrintAllAuto())
            {
                printedList = listToPrint;
                RemovePrintedItems();
                displayedList = displayedList.Except(printedList).ToList();
                UpdateDataGrid(displayedList);
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Η ποσότητα πρέπει να είναι μεταξύ 1 και 99999");
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            string text = comboBox1.Text;
            /*if (text.Length == 2)
            {
                
            }*/
            if (e.KeyCode == Keys.Enter)
            {
                if (System.Text.RegularExpressions.Regex.Matches(text, @"[a-zA-Z]").Count == 0)
                {
                    if (backgroundWorkerBarcodeFinder.IsBusy)
                    {
                        BackgroundWorker backgroundCodeFinder = new BackgroundWorker();
                        backgroundCodeFinder.DoWork += backgroundWorkerBarcodeFinder_DoWork;
                        backgroundCodeFinder.RunWorkerCompleted += backgroundWorkerBarcodeFinder_RunWorkerCompleted;
                        backgroundCodeFinder.RunWorkerAsync(text);
                    }
                    else
                        backgroundWorkerBarcodeFinder.RunWorkerAsync(text);
                }
                else
                {
                    var item = (DatabaseProduct)comboBox1.SelectedItem;
                    if (item != null)
                    {
                        var foundProduct = new OrderedProduct();
                        foundProduct.AdditionTime = DateTime.Now;
                        foundProduct.Name = item.Description + " " + item.Morfi;
                        foundProduct.HasEOFCode = item.KodikosEOF != null && item.KodikosEOF != string.Empty;
                        var barcodes = item.BarcodeS.FirstOrDefault();
                        if (barcodes != null)
                            foundProduct.Barcode = barcodes.Barcode;//result.BarcodeS.FirstOrDefault().Barcode;
                        else
                        {
                            foundProduct.Barcode = "0000000000000";
                            MessageBox.Show("ΠΡΟΣΟΧΉ: Το είδος δεν έχει barcode");
                        }
                        foundProduct.Price = item.TimiXondrikis;
                        foundProduct.Code = item.KodikosApothikis;
                        AddFoundProduct(foundProduct);
                    }
                }
                /*
            else
            {
                if (backgroundWorkerNameFinder.IsBusy)
                {
                    BackgroundWorker backgroundNameFinder = new BackgroundWorker();
                    backgroundNameFinder.DoWork += backgroundWorkerNameFinder_DoWork;
                    backgroundNameFinder.RunWorkerCompleted += backgroundWorkerNameFinder_RunWorkerCompleted;
                    backgroundNameFinder.RunWorkerAsync(text);
                }
                else
                    backgroundWorkerNameFinder.RunWorkerAsync(text);
            }*/
                e.SuppressKeyPress = true;
                comboBox1.Focus();
            }
        }

        void StartSaveThread()
        {
            if (backgroundWorkerSave.IsBusy)
            {
                BackgroundWorker newBackgroundWorkerSave = new BackgroundWorker();
                newBackgroundWorkerSave.DoWork += backgroundWorkerSave_DoWork;
                newBackgroundWorkerSave.RunWorkerAsync();
            }
            else
                backgroundWorkerSave.RunWorkerAsync();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if ((senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn) && e.RowIndex >= 0)
            {
                orderList.Remove((OrderedProduct)senderGrid.Rows[e.RowIndex].DataBoundItem);
                UpdateDataGrid(orderList);
                StartSaveThread();
            }
        }

        private void εγκατάστασηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstallationForm n = new InstallationForm();
            n.ShowDialog();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.WindowSize = this.Size;
            else
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.Save();
        }

        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        const int KEY_DOWN_EVENT = 0x0001; //Key down flag
        const int KEY_UP_EVENT = 0x0002; //Key up flag

        public static void HoldKey(byte key, int duration)
        {
            const int PauseBetweenStrokes = 50;
            int totalDuration = 0;
            while (totalDuration < duration)
            {
                keybd_event(key, 0, KEY_DOWN_EVENT, 0);
                keybd_event(key, 0, KEY_UP_EVENT, 0);
                System.Threading.Thread.Sleep(PauseBetweenStrokes);
                totalDuration += PauseBetweenStrokes;
            }
        }

        private void sendToFarmakon(object sender, EventArgs e)
        {
            Forms.WindowSelectionForm ws = new Forms.WindowSelectionForm();

            ws.ShowDialog();

            if (ApplicationIsActivated())
            {
                return;
            }

            printedList = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).ToList();

            //            var farmakonWindowRect = GetFarmakonSize("farmakoNetSQL.exe");

            var targetHWND = GetForegroundWindow();
            //this.textBox1.Text = "Current HWND:  " + targetHWND;

            foreach (var P in printedList)
            {
                //try to send it 10 times
                //for (int j = 0; j < 10; j++)
                //{
                //var startingScreenshot = ScreenshotCurrentWindow(farmakonWindowRect);
                for (int i = 0; i < P.Quantity; i++)
                {
                    //if (P.Barcode == "0000000000000" || j > 0) //if it's not the first attempt, try with the code
                    if (P.Barcode == "0000000000000")
                    {
                        SendKeys.SendWait(P.Code);
                    }
                    else
                    {
                        SendKeys.SendWait(P.Barcode);
                    }
                    Thread.Sleep(50);
                    SendKeys.SendWait("{ENTER}");
                    Thread.Sleep(1000);
                    if (targetHWND != GetForegroundWindow())
                    {
                        //this.textBox1.Text += "\nHWND changed to:  " + GetForegroundWindow();
                        SendKeys.SendWait("{ENTER}");
                        Thread.Sleep(500);
                    }
                    //if (P.Quantity > 1)
                    //{
                    //Thread.Sleep(1000);
                    //SendKeys.SendWait(P.Quantity.ToString());
                    //Thread.Sleep(200);
                    //}
                    HoldKey((byte)Keys.Enter, 1000);

                }
                //var endingScreenshot = ScreenshotCurrentWindow(farmakonWindowRect);
                //If the screenshot has changed
                //if (CompareBitmapsFast(startingScreenshot, endingScreenshot) == false)
                //{
                //stop repeating
                //  break;
                //}
                //}
            }
            RemovePrintedItems();
            displayedList = displayedList.Except(printedList).ToList();
            UpdateDataGrid(displayedList);
        }

        private void αποστολήΣεFarmakonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //System.Timers.Timer tim = new System.Timers.Timer(10);
            //
            /*
            Process farnet = Process.GetProcessById(7788); //.GetProcessesByName("notepad++");
            if (farnet == null)
            {
                MessageBox.Show("Δεν βρέθηκε το FarmakoNet SQL");
                return;
            }
            IntPtr farmakonHandle = farnet.Handle;
            //IntPtr farmakonHandle = FindWindow(null, "Notepad++");
            Process currentProcess = Process.GetCurrentProcess();
            IntPtr currentProcessHandle = currentProcess.MainWindowHandle;
            if (farmakonHandle == IntPtr.Zero)
            {
                MessageBox.Show("Δεν βρέθηκε το handle του Farmakon.NET SQL");
                return;
            }
            SetForegroundWindow(farmakonHandle);*/
            //if (dataGridView1.IsCurrentRowDirty)
            //    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            for (int i = 0; i < 100; i++)
            {
                SendKeys.SendWait("%{TAB}");
                if (!ApplicationIsActivated())
                    break;
            }

            if (ApplicationIsActivated())
            {
                MessageBox.Show("Δυστυχώς η αποστολή απέτυχε. :( ");
                return;
            }

            printedList = displayedList.Where(x => x.HasEOFCode == (tabControl1.SelectedIndex == 0)).ToList();

            var farmakonWindowRect = GetFarmakonSize("farmakoNetSQL.exe");

            foreach (var P in printedList)
            {
                //try to send it 10 times
                for (int j = 0; j < 10; j++)
                {
                    var startingScreenshot = ScreenshotCurrentWindow(farmakonWindowRect);
                    for (int i = 0; i < P.Quantity; i++)
                    {
                        if (P.Barcode == "0000000000000" || j > 0) //if it's not the first attempt, try with the code
                            SendKeys.SendWait(P.Code);
                        else
                            SendKeys.SendWait(P.Barcode);
                        Thread.Sleep(100);
                        SendKeys.SendWait("{ENTER}");
                        Thread.Sleep(100);
                    }
                    var endingScreenshot = ScreenshotCurrentWindow(farmakonWindowRect);
                    //If the screenshot has changed
                    if (CompareBitmapsFast(startingScreenshot, endingScreenshot) == false)
                    {
                        //stop repeating
                        break;
                    }
                }
            }
            RemovePrintedItems();
            displayedList = displayedList.Except(printedList).ToList();
            UpdateDataGrid(displayedList);
            /*
            foreach(OrderedProduct p in displayedList)
            {
                SendKeys.SendWait(p.Barcode);
                System.Threading.Thread.Sleep(10);
                SendKeys.SendWait("{ENTER}");
                System.Threading.Thread.Sleep(10);
            }*/
            //SendKeys.SendWait("{F3}");
            //SetForegroundWindow(currentProcessHandle);
            //MessageBox.Show("Ολοκληρώθηκε!"+farnet[0].MainWindowTitle);
        }

        private IntPtr FindFarmakonhWnd(string farmakonProcessTitle)
        {
            Process[] processes = Process.GetProcessesByName(farmakonProcessTitle);
            // if there is more than one process...
            if (processes.Length > 1)
            {
                return processes[0].MainWindowHandle;
            }
            return IntPtr.Zero;
        }

        /// <summary>
        ///  Check if two bitmap objects are the same or not
        /// </summary>
        /// <see cref="http://csharpexamples.com/c-fast-bitmap-compare/"/>
        /// <returns>True if they are the same</returns>
        public static bool CompareBitmapsFast(Bitmap bmp1, Bitmap bmp2)
        {
            if (bmp1 == null || bmp2 == null)
                return false;
            if (object.Equals(bmp1, bmp2))
                return true;
            if (!bmp1.Size.Equals(bmp2.Size) || !bmp1.PixelFormat.Equals(bmp2.PixelFormat))
                return false;

            int bytes = bmp1.Width * bmp1.Height * (Image.GetPixelFormatSize(bmp1.PixelFormat) / 8);

            bool result = true;
            byte[] b1bytes = new byte[bytes];
            byte[] b2bytes = new byte[bytes];

            System.Drawing.Imaging.BitmapData bitmapData1 = bmp1.LockBits(new Rectangle(0, 0, bmp1.Width - 1, bmp1.Height - 1), System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp1.PixelFormat);
            System.Drawing.Imaging.BitmapData bitmapData2 = bmp2.LockBits(new Rectangle(0, 0, bmp2.Width - 1, bmp2.Height - 1), System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp2.PixelFormat);

            Marshal.Copy(bitmapData1.Scan0, b1bytes, 0, bytes);
            Marshal.Copy(bitmapData2.Scan0, b2bytes, 0, bytes);

            for (int n = 0; n <= bytes - 1; n++)
            {
                if (b1bytes[n] != b2bytes[n])
                {
                    result = false;
                    break;
                }
            }

            bmp1.UnlockBits(bitmapData1);
            bmp2.UnlockBits(bitmapData2);

            return result;
        }

        //private bool SetFocusToFarmacon(string farmakonProcessTitle)
        //{
        //    IntPtr farmakonhWnd = FindFarmakonhWnd(farmakonProcessTitle);
        //    if (IsIconic(farmakonhWnd))
        //    {
        //        ShowWindowAsync(farmakonhWnd, SW_RESTORE);
        //    }
        //    SetForegroundWindow(farmakonhWnd);
        //}

        private Bitmap ScreenshotCurrentWindow(Tuple<int, int, User32.Rect> widthHeight)
        {
            //Rectangle bounds = this.Bounds;
            //using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            //{
            //    using (Graphics g = Graphics.FromImage(bitmap))
            //    {
            //        g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
            //    }
            //    bitmap.Save("C://test.jpg", ImageFormat.Jpeg);
            //}

            var bmp = new Bitmap(widthHeight.Item1, widthHeight.Item2, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(widthHeight.Item3.left, widthHeight.Item3.top, 0, 0, new Size(widthHeight.Item1, widthHeight.Item2), CopyPixelOperation.SourceCopy);
            return bmp;
        }

        private Tuple<int, int, User32.Rect> GetFarmakonSize(string farmakonProcessTitle)
        {
            //http://stackoverflow.com/questions/891345/get-a-screenshot-of-a-specific-application
            var proc = Process.GetProcessesByName(farmakonProcessTitle)[0];
            var rect = new User32.Rect();
            User32.GetWindowRect(proc.MainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            return new Tuple<int, int, User32.Rect>(width, height, rect);
        }

        private void backgroundWorkerNameFinder_DoWork(object sender, DoWorkEventArgs e)
        {
            string nameToLookup = e.Argument.ToString();

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            List<DatabaseProduct> result = new List<DatabaseProduct>();
            try { result = db.GetTable<DatabaseProduct>().Where(x => x.Description.StartsWith(nameToLookup)).ToList(); }
            catch (Exception)
            { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

            if (result.Count() > 0)
            {
                var foundProducts = new List<OrderedProduct>();
                foreach (var prod in result)
                {
                    var newProd = new OrderedProduct();
                    newProd.AdditionTime = DateTime.Now;
                    newProd.Name = prod.Description + " " + prod.Morfi;
                    newProd.HasEOFCode = prod.KodikosEOF != null && prod.KodikosEOF != string.Empty;
                    var a = prod.BarcodeS.FirstOrDefault();
                    if (a != null)
                        newProd.Barcode = a.Barcode;
                    else
                        continue;
                    newProd.Price = prod.TimiXondrikis;
                    newProd.Code = prod.KodikosApothikis;
                    foundProducts.Add(newProd);
                }
                e.Result = foundProducts;
            }
            else
            {
                try { result = db.GetTable<DatabaseProduct>().Where(x => x.BarcodeS.Any(y => y.Barcode == nameToLookup)).ToList(); }
                catch (Exception)
                { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

                if (result != null)
                {
                    var tempProd = result.FirstOrDefault();
                    if (tempProd != null)
                    {
                        var foundProduct = new OrderedProduct();
                        foundProduct.AdditionTime = DateTime.Now;
                        foundProduct.Name = tempProd.Description + " " + tempProd.Morfi;
                        foundProduct.Barcode = nameToLookup;
                        foundProduct.Price = tempProd.TimiXondrikis;
                        foundProduct.Code = tempProd.KodikosApothikis;
                        foundProduct.HasEOFCode = tempProd.KodikosEOF != null && tempProd.KodikosEOF != string.Empty;
                        var tempList = new List<OrderedProduct>();
                        tempList.Add(foundProduct);
                        e.Result = tempList;
                    }
                    else
                        e.Result = null;
                }
                else
                    e.Result = null;
            }
        }

        private void backgroundWorkerNameFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("To είδος δεν βρέθηκε!");
                comboBox1.SelectAll();
                return;
            }
            comboBox1.DataSource = ((List<OrderedProduct>)e.Result).Select(x => x.Name).ToList();
            /*List<OrderedProduct> tempList = (List<OrderedProduct>)e.Result;
            if (tempList.Count == 1)
            {
                orderList.Add(tempList.First());
                UpdateDataGrid(orderList);
                comboBox1.Items.Clear();
                comboBox1.Text = "";
                var productInOrder = orderList.Find(x => x.Barcode == tempList.First().Barcode);
                if (productInOrder != null)
                {
                    productInOrder.AdditionTime = DateTime.Now;
                    orderList = orderList.OrderBy(x => x.Barcode == productInOrder.Barcode).ToList();
                }
                else
                {
                    orderList.Add(tempList.First());
                }
                StartSaveThread();
                UpdateDataGrid(orderList);
                comboBox1.Items.Clear();
                comboBox1.Text = "";
                if (backgroundWorkerBarcodeFinder.IsBusy == false)
                {
                    string rest = barcodesToLookupQueue.FirstOrDefault();
                    if (rest != null)
                    {
                        barcodesToLookupQueue.RemoveAt(0);
                        backgroundWorkerBarcodeFinder.RunWorkerAsync(rest);
                    }
                }
            }
            else
            {
                comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                comboBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
                var autoCompleteList = new AutoCompleteStringCollection();
                autoCompleteList.AddRange(tempList.Select(x => x.Name).ToArray());
                comboBox1.AutoCompleteCustomSource = autoCompleteList;
            }*/
        }

        private void εκτυπωμένεςΠαραγγελίεςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(PrintedDir))
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(), PrintedDir);
                DialogResult res = fd.ShowDialog();
                if (res == DialogResult.OK)
                {
                    using (var file = fd.OpenFile())
                    {
                        printedList = ProtoBuf.Serializer.Deserialize<List<OrderedProduct>>(file);
                    }
                    UpdateDataGrid(printedList);
                    dataGridView1.Columns["Delete"].Visible = false;
                    dataGridView2.Columns["dataGridViewCheckBoxColumn1"].Visible = false;
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTextBoxTotals();
            textBox2.BackColor = (tabControl1.SelectedIndex == 0) ? Properties.Settings.Default.FarmakaSumsColor : Properties.Settings.Default.ParafarmakaSumsColor;
            textBoxEidi.BackColor = (tabControl1.SelectedIndex == 0) ? Properties.Settings.Default.FarmakaSumsColor : Properties.Settings.Default.ParafarmakaSumsColor;
            textBoxTemaxia.BackColor = (tabControl1.SelectedIndex == 0) ? Properties.Settings.Default.FarmakaSumsColor : Properties.Settings.Default.ParafarmakaSumsColor;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            MessageBox.Show("dasda");
        }

        private void textBox1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                var input = textBoxMoney.Text;
                input = input.Replace(".", ",");
                var splitInput = input.Split('+');
                //if (splitInput[0].Length > 1 || splitInput[1].Length <1)
                //{
                //    MessageBox.Show("Λάθος εισαγωγή χρημάτων");
                //    textBoxMoney.Text = "";
                //    return;
                //}
                decimal value = 0;
                if (splitInput[0].Length > 0)
                {
                    if (!Decimal.TryParse(splitInput[0], out value))
                    {
                        textBoxMoney.Clear();
                        MessageBox.Show("Μη έγκυρο ποσό");
                        return;
                    }
                    value = -value;
                }
                if (splitInput.Length > 1 && splitInput[1].Length > 0)
                {
                    decimal prevValue = 0;
                    if (value != 0)
                    {
                        prevValue = -value;
                    }
                    if (!Decimal.TryParse(splitInput[1], out value))
                    {
                        textBoxMoney.Clear();
                        MessageBox.Show("Μη έγκυρο ποσό");
                        return;
                    }
                    value += prevValue;
                }
                if (value == 0)
                {
                    textBoxMoney.Clear();
                    return;
                }

                tameio += value;

                var result = cashierSumList.Where(x => x.date == DateTime.Today).FirstOrDefault();
                if (result == null)
                    cashierSumList.Add(new CashierSum(tameio));
                else
                    result.Sum = tameio;
                SaveSum();
                textBoxResultMoney.Text = tameio.ToString("C");
                textBoxMoney.Clear();
                comboBox1.Focus();
            }
        }

        private void ταμείοToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.CashierColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                textBoxResultMoney.BackColor = colorDialog1.Color;
            }
        }

        private void σύνολαΤεμάχιαΕίδηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.FarmakaSumsColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                if (tabControl1.SelectedIndex == 0)
                {
                    textBox2.BackColor = colorDialog1.Color;
                    textBoxEidi.BackColor = colorDialog1.Color;
                    textBoxTemaxia.BackColor = colorDialog1.Color;
                }
            }
        }

        private void περιττέςΓραμμέςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.FarmakaDVGMainColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                dataGridView1.RowsDefaultCellStyle.BackColor = colorDialog1.Color;
            }
        }

        private void άρτιεςΓραμμέςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.FarmakaDVGAltColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = colorDialog1.Color;
            }
        }

        private void σύνολαΤεμάχιαΕίδηToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.ParafarmakaSumsColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                if (tabControl1.SelectedIndex == 1)
                {
                    textBox2.BackColor = colorDialog1.Color;
                    textBoxEidi.BackColor = colorDialog1.Color;
                    textBoxTemaxia.BackColor = colorDialog1.Color;
                }
            }
        }

        private void περιττέςΓραμμέςToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.ParafarmakaDVGMainColor = colorDialog1.Color;
                Properties.Settings.Default.Save();
                dataGridView2.RowsDefaultCellStyle.BackColor = colorDialog1.Color;
            }
        }

        private void άρτιεςΓραμμέςToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.ParafarmakaDVGAltColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                dataGridView2.AlternatingRowsDefaultCellStyle.BackColor = colorDialog1.Color;
            }
        }

        private void υπόλοιπαToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.CashierInputColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                textBoxMoney.BackColor = colorDialog1.Color;
            }
        }

        private void barcodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.BarcodeColor = colorDialog1.Color;
                Properties.Settings.Default.Save();

                comboBox1.BackColor = colorDialog1.Color;
            }
        }
        private class User32
        {
            [StructLayout(LayoutKind.Sequential)]
            public struct Rect
            {
                public int left;
                public int top;
                public int right;
                public int bottom;
            }

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
        }
    }
}
