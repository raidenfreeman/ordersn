﻿namespace Orders
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.αποθήκευσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.φόρτωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αλλαγήΒάσηςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εγκατάστασηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αποστολήΣεFarmakonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αλλαγήΧρώματοςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ταμείοToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.φάρμακαToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.περιττέςΓραμμέςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.άρτιεςΓραμμέςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.παραφάρμακαToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.περιττέςΓραμμέςToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.άρτιεςΓραμμέςToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.υπόλοιπαToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.labelBarcode = new System.Windows.Forms.Label();
            this.backgroundWorkerBarcodeFinder = new System.ComponentModel.BackgroundWorker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.orderedProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.backgroundWorkerSave = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerLoad = new System.ComponentModel.BackgroundWorker();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxEidi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTemaxia = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMoney = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxResultMoney = new System.Windows.Forms.TextBox();
            this.backgroundWorkerNameFinder = new System.ComponentModel.BackgroundWorker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.orderableProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderableProductBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderedProductBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.αποθήκευσηToolStripMenuItem,
            this.φόρτωσηToolStripMenuItem,
            this.αλλαγήΒάσηςToolStripMenuItem,
            this.εκτύπωσηToolStripMenuItem,
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem,
            this.εγκατάστασηToolStripMenuItem,
            this.αποστολήΣεFarmakonToolStripMenuItem,
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem,
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem,
            this.αλλαγήΧρώματοςToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(10, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1261, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // αποθήκευσηToolStripMenuItem
            // 
            this.αποθήκευσηToolStripMenuItem.Name = "αποθήκευσηToolStripMenuItem";
            this.αποθήκευσηToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.αποθήκευσηToolStripMenuItem.Text = "Αποθήκευση";
            this.αποθήκευσηToolStripMenuItem.Click += new System.EventHandler(this.αποθήκευσηToolStripMenuItem_Click);
            // 
            // φόρτωσηToolStripMenuItem
            // 
            this.φόρτωσηToolStripMenuItem.Name = "φόρτωσηToolStripMenuItem";
            this.φόρτωσηToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.φόρτωσηToolStripMenuItem.Text = "Φόρτωση";
            this.φόρτωσηToolStripMenuItem.Click += new System.EventHandler(this.φόρτωσηToolStripMenuItem_Click_1);
            // 
            // αλλαγήΒάσηςToolStripMenuItem
            // 
            this.αλλαγήΒάσηςToolStripMenuItem.Name = "αλλαγήΒάσηςToolStripMenuItem";
            this.αλλαγήΒάσηςToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.αλλαγήΒάσηςToolStripMenuItem.Text = "Αλλαγή Βάσης";
            this.αλλαγήΒάσηςToolStripMenuItem.Click += new System.EventHandler(this.αλλαγήΒάσηςToolStripMenuItem_Click);
            // 
            // εκτύπωσηToolStripMenuItem
            // 
            this.εκτύπωσηToolStripMenuItem.Name = "εκτύπωσηToolStripMenuItem";
            this.εκτύπωσηToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.εκτύπωσηToolStripMenuItem.Text = "Εκτύπωση";
            this.εκτύπωσηToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηToolStripMenuItem_Click);
            // 
            // εκτύπωσηΣεΠροεπιλογήToolStripMenuItem
            // 
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Name = "εκτύπωσηΣεΠροεπιλογήToolStripMenuItem";
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Size = new System.Drawing.Size(159, 20);
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Text = "Εκτύπωση Σε Προεπιλογή";
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem_Click);
            // 
            // εγκατάστασηToolStripMenuItem
            // 
            this.εγκατάστασηToolStripMenuItem.Name = "εγκατάστασηToolStripMenuItem";
            this.εγκατάστασηToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.εγκατάστασηToolStripMenuItem.Text = "Εγκατάσταση";
            this.εγκατάστασηToolStripMenuItem.Click += new System.EventHandler(this.εγκατάστασηToolStripMenuItem_Click);
            // 
            // αποστολήΣεFarmakonToolStripMenuItem
            // 
            this.αποστολήΣεFarmakonToolStripMenuItem.Name = "αποστολήΣεFarmakonToolStripMenuItem";
            this.αποστολήΣεFarmakonToolStripMenuItem.Size = new System.Drawing.Size(146, 20);
            this.αποστολήΣεFarmakonToolStripMenuItem.Text = "Αποστολή σε Farmakon";
            this.αποστολήΣεFarmakonToolStripMenuItem.Click += new System.EventHandler(this.sendToFarmakon);
            // 
            // εκτυπωμένεςΠαραγγελίεςToolStripMenuItem
            // 
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Name = "εκτυπωμένεςΠαραγγελίεςToolStripMenuItem";
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Size = new System.Drawing.Size(162, 20);
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Text = "Εκτυπωμένες Παραγγελίες";
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Click += new System.EventHandler(this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem_Click);
            // 
            // εμφάνισηΑνατιμήσεωνToolStripMenuItem
            // 
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem.Name = "εμφάνισηΑνατιμήσεωνToolStripMenuItem";
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem.Size = new System.Drawing.Size(147, 20);
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem.Text = "Εμφάνιση Ανατιμήσεων";
            this.εμφάνισηΑνατιμήσεωνToolStripMenuItem.Visible = false;
            // 
            // αλλαγήΧρώματοςToolStripMenuItem
            // 
            this.αλλαγήΧρώματοςToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ταμείοToolStripMenuItem,
            this.φάρμακαToolStripMenuItem1,
            this.παραφάρμακαToolStripMenuItem1,
            this.υπόλοιπαToolStripMenuItem,
            this.barcodeToolStripMenuItem});
            this.αλλαγήΧρώματοςToolStripMenuItem.Name = "αλλαγήΧρώματοςToolStripMenuItem";
            this.αλλαγήΧρώματοςToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.αλλαγήΧρώματοςToolStripMenuItem.Text = "Αλλαγή Χρώματος";
            // 
            // ταμείοToolStripMenuItem
            // 
            this.ταμείοToolStripMenuItem.Name = "ταμείοToolStripMenuItem";
            this.ταμείοToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ταμείοToolStripMenuItem.Text = "Ταμείο";
            this.ταμείοToolStripMenuItem.Click += new System.EventHandler(this.ταμείοToolStripMenuItem_Click);
            // 
            // φάρμακαToolStripMenuItem1
            // 
            this.φάρμακαToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem,
            this.περιττέςΓραμμέςToolStripMenuItem,
            this.άρτιεςΓραμμέςToolStripMenuItem});
            this.φάρμακαToolStripMenuItem1.Name = "φάρμακαToolStripMenuItem1";
            this.φάρμακαToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.φάρμακαToolStripMenuItem1.Text = "Φάρμακα";
            // 
            // σύνολαΤεμάχιαΕίδηToolStripMenuItem
            // 
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem.Name = "σύνολαΤεμάχιαΕίδηToolStripMenuItem";
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem.Text = "Σύνολα/Τεμάχια/Είδη";
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem.Click += new System.EventHandler(this.σύνολαΤεμάχιαΕίδηToolStripMenuItem_Click);
            // 
            // περιττέςΓραμμέςToolStripMenuItem
            // 
            this.περιττέςΓραμμέςToolStripMenuItem.Name = "περιττέςΓραμμέςToolStripMenuItem";
            this.περιττέςΓραμμέςToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.περιττέςΓραμμέςToolStripMenuItem.Text = "Περιττές Γραμμές";
            this.περιττέςΓραμμέςToolStripMenuItem.Click += new System.EventHandler(this.περιττέςΓραμμέςToolStripMenuItem_Click);
            // 
            // άρτιεςΓραμμέςToolStripMenuItem
            // 
            this.άρτιεςΓραμμέςToolStripMenuItem.Name = "άρτιεςΓραμμέςToolStripMenuItem";
            this.άρτιεςΓραμμέςToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.άρτιεςΓραμμέςToolStripMenuItem.Text = "Άρτιες Γραμμές";
            this.άρτιεςΓραμμέςToolStripMenuItem.Click += new System.EventHandler(this.άρτιεςΓραμμέςToolStripMenuItem_Click);
            // 
            // παραφάρμακαToolStripMenuItem1
            // 
            this.παραφάρμακαToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1,
            this.περιττέςΓραμμέςToolStripMenuItem1,
            this.άρτιεςΓραμμέςToolStripMenuItem1});
            this.παραφάρμακαToolStripMenuItem1.Name = "παραφάρμακαToolStripMenuItem1";
            this.παραφάρμακαToolStripMenuItem1.Size = new System.Drawing.Size(154, 22);
            this.παραφάρμακαToolStripMenuItem1.Text = "Παραφάρμακα";
            // 
            // σύνολαΤεμάχιαΕίδηToolStripMenuItem1
            // 
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1.Name = "σύνολαΤεμάχιαΕίδηToolStripMenuItem1";
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1.Text = "Σύνολα/Τεμάχια/Είδη";
            this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1.Click += new System.EventHandler(this.σύνολαΤεμάχιαΕίδηToolStripMenuItem1_Click);
            // 
            // περιττέςΓραμμέςToolStripMenuItem1
            // 
            this.περιττέςΓραμμέςToolStripMenuItem1.Name = "περιττέςΓραμμέςToolStripMenuItem1";
            this.περιττέςΓραμμέςToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.περιττέςΓραμμέςToolStripMenuItem1.Text = "Περιττές Γραμμές";
            this.περιττέςΓραμμέςToolStripMenuItem1.Click += new System.EventHandler(this.περιττέςΓραμμέςToolStripMenuItem1_Click);
            // 
            // άρτιεςΓραμμέςToolStripMenuItem1
            // 
            this.άρτιεςΓραμμέςToolStripMenuItem1.Name = "άρτιεςΓραμμέςToolStripMenuItem1";
            this.άρτιεςΓραμμέςToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.άρτιεςΓραμμέςToolStripMenuItem1.Text = "Άρτιες Γραμμές";
            this.άρτιεςΓραμμέςToolStripMenuItem1.Click += new System.EventHandler(this.άρτιεςΓραμμέςToolStripMenuItem1_Click);
            // 
            // υπόλοιπαToolStripMenuItem
            // 
            this.υπόλοιπαToolStripMenuItem.Name = "υπόλοιπαToolStripMenuItem";
            this.υπόλοιπαToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.υπόλοιπαToolStripMenuItem.Text = "Υπόλοιπα";
            this.υπόλοιπαToolStripMenuItem.Click += new System.EventHandler(this.υπόλοιπαToolStripMenuItem_Click);
            // 
            // barcodeToolStripMenuItem
            // 
            this.barcodeToolStripMenuItem.Name = "barcodeToolStripMenuItem";
            this.barcodeToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.barcodeToolStripMenuItem.Text = "Barcode";
            this.barcodeToolStripMenuItem.Click += new System.EventHandler(this.barcodeToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(654, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 72);
            this.button1.TabIndex = 4;
            this.button1.TabStop = false;
            this.button1.Text = "Προβολή πωλήσεων";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelBarcode
            // 
            this.labelBarcode.AutoSize = true;
            this.labelBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBarcode.Location = new System.Drawing.Point(3, 0);
            this.labelBarcode.Name = "labelBarcode";
            this.labelBarcode.Size = new System.Drawing.Size(245, 34);
            this.labelBarcode.TabIndex = 7;
            this.labelBarcode.Text = "Barcode";
            this.labelBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorkerBarcodeFinder
            // 
            this.backgroundWorkerBarcodeFinder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerBarcodeFinder_DoWork);
            this.backgroundWorkerBarcodeFinder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerBarcodeFinder_RunWorkerCompleted);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.quantityDataGridViewTextBoxColumn,
            this.Code,
            this.nameDataGridViewTextBoxColumn,
            this.Price,
            this.Total,
            this.barcodeDataGridViewTextBoxColumn,
            this.Delete});
            this.dataGridView1.DataSource = this.orderedProductBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1247, 639);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "ΤΕΜΑΧΙΑ";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.Width = 104;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "ΚΩΔΙΚΟΣ";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            this.Code.Width = 105;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "ΠΕΡΙΓΡΑΦΗ";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 120;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Price.DefaultCellStyle = dataGridViewCellStyle4;
            this.Price.HeaderText = "ΤΙΜΗ ΜΟΝΑΔΑΣ";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 161;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle5.Format = "C2";
            dataGridViewCellStyle5.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle5;
            this.Total.HeaderText = "ΣΥΝΟΛΟ";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            this.barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.HeaderText = "BARCODE";
            this.barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            this.barcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.barcodeDataGridViewTextBoxColumn.Width = 108;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "ΔΙΑΓΡΑΦΗ";
            this.Delete.Name = "Delete";
            this.Delete.Width = 93;
            // 
            // orderedProductBindingSource
            // 
            this.orderedProductBindingSource.DataSource = typeof(Orders.OrderedProduct);
            // 
            // backgroundWorkerSave
            // 
            this.backgroundWorkerSave.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerSave_DoWork);
            // 
            // backgroundWorkerLoad
            // 
            this.backgroundWorkerLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerLoad_DoWork);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(256, 39);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(171, 32);
            this.textBox2.TabIndex = 1;
            this.textBox2.TabStop = false;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Lucida Sans Unicode", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(254, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 34);
            this.label1.TabIndex = 10;
            this.label1.Text = "Σύνολο Χονδρικής";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.66667F));
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelBarcode, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(432, 88);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 40);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(245, 28);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 28);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1117, 92);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.textBoxEidi, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBoxTemaxia, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(441, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.37209F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.62791F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 86);
            this.tableLayoutPanel4.TabIndex = 11;
            // 
            // textBoxEidi
            // 
            this.textBoxEidi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEidi.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBoxEidi.ForeColor = System.Drawing.Color.White;
            this.textBoxEidi.Location = new System.Drawing.Point(105, 43);
            this.textBoxEidi.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxEidi.Name = "textBoxEidi";
            this.textBoxEidi.ReadOnly = true;
            this.textBoxEidi.Size = new System.Drawing.Size(90, 32);
            this.textBoxEidi.TabIndex = 12;
            this.textBoxEidi.TabStop = false;
            this.textBoxEidi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Είδη";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Τεμάχια";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxTemaxia
            // 
            this.textBoxTemaxia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTemaxia.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBoxTemaxia.ForeColor = System.Drawing.Color.White;
            this.textBoxTemaxia.Location = new System.Drawing.Point(5, 43);
            this.textBoxTemaxia.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTemaxia.Name = "textBoxTemaxia";
            this.textBoxTemaxia.ReadOnly = true;
            this.textBoxTemaxia.Size = new System.Drawing.Size(90, 32);
            this.textBoxTemaxia.TabIndex = 9;
            this.textBoxTemaxia.TabStop = false;
            this.textBoxTemaxia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(817, 2);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(297, 88);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBoxMoney, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(291, 38);
            this.tableLayoutPanel6.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 38);
            this.label4.TabIndex = 11;
            this.label4.Text = "Υπόλοιπα";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxMoney
            // 
            this.textBoxMoney.BackColor = System.Drawing.Color.Plum;
            this.textBoxMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMoney.Location = new System.Drawing.Point(99, 3);
            this.textBoxMoney.Name = "textBoxMoney";
            this.textBoxMoney.Size = new System.Drawing.Size(189, 32);
            this.textBoxMoney.TabIndex = 6;
            this.textBoxMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown_1);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBoxResultMoney, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 47);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(291, 38);
            this.tableLayoutPanel5.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 38);
            this.label5.TabIndex = 14;
            this.label5.Text = "Ταμείο";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxResultMoney
            // 
            this.textBoxResultMoney.BackColor = System.Drawing.Color.Yellow;
            this.textBoxResultMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxResultMoney.ForeColor = System.Drawing.Color.White;
            this.textBoxResultMoney.Location = new System.Drawing.Point(75, 5);
            this.textBoxResultMoney.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxResultMoney.Name = "textBoxResultMoney";
            this.textBoxResultMoney.ReadOnly = true;
            this.textBoxResultMoney.Size = new System.Drawing.Size(211, 32);
            this.textBoxResultMoney.TabIndex = 13;
            this.textBoxResultMoney.TabStop = false;
            this.textBoxResultMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // backgroundWorkerNameFinder
            // 
            this.backgroundWorkerNameFinder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerNameFinder_DoWork);
            this.backgroundWorkerNameFinder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerNameFinder_RunWorkerCompleted);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 120);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1261, 678);
            this.tabControl1.TabIndex = 9;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1253, 645);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Φάρμακα";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1253, 645);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Παραφάρμακα";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewCheckBoxColumn1});
            this.dataGridView2.DataSource = this.orderedProductBindingSource;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1247, 639);
            this.dataGridView2.TabIndex = 9;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Quantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn1.HeaderText = "ΤΕΜΑΧΙΑ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 104;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Code";
            this.dataGridViewTextBoxColumn2.HeaderText = "ΚΩΔΙΚΟΣ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 105;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn3.HeaderText = "ΠΕΡΙΓΡΑΦΗ";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 120;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Price";
            dataGridViewCellStyle10.Format = "C2";
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn4.HeaderText = "ΤΙΜΗ ΜΟΝΑΔΑΣ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 161;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Total";
            dataGridViewCellStyle11.Format = "C2";
            dataGridViewCellStyle11.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn5.HeaderText = "ΣΥΝΟΛΟ";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Barcode";
            this.dataGridViewTextBoxColumn6.HeaderText = "BARCODE";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 108;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "ΔΙΑΓΡΑΦΗ";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 93;
            // 
            // orderableProductBindingSource
            // 
            this.orderableProductBindingSource.DataSource = typeof(Orders.DatabaseProduct);
            // 
            // orderableProductBindingSource1
            // 
            this.orderableProductBindingSource1.DataSource = typeof(Orders.DatabaseProduct);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1261, 798);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MainForm";
            this.Text = "Παραγγελίες";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderedProductBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem αποθήκευσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αλλαγήΒάσηςToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelBarcode;
        private System.ComponentModel.BackgroundWorker backgroundWorkerBarcodeFinder;
        private System.Windows.Forms.BindingSource orderableProductBindingSource;
        private System.Windows.Forms.BindingSource orderableProductBindingSource1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource orderedProductBindingSource;
        private System.ComponentModel.BackgroundWorker backgroundWorkerSave;
        private System.ComponentModel.BackgroundWorker backgroundWorkerLoad;
        private System.Windows.Forms.ToolStripMenuItem φόρτωσηToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηΣεΠροεπιλογήToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εγκατάστασηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αποστολήΣεFarmakonToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerNameFinder;
        private System.Windows.Forms.ToolStripMenuItem εκτυπωμένεςΠαραγγελίεςToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox textBoxEidi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTemaxia;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Delete;
        private System.Windows.Forms.ToolStripMenuItem εμφάνισηΑνατιμήσεωνToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxMoney;
        private System.Windows.Forms.TextBox textBoxResultMoney;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem αλλαγήΧρώματοςToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripMenuItem ταμείοToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem φάρμακαToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem σύνολαΤεμάχιαΕίδηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem περιττέςΓραμμέςToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem άρτιεςΓραμμέςToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem παραφάρμακαToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem σύνολαΤεμάχιαΕίδηToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem περιττέςΓραμμέςToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem άρτιεςΓραμμέςToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem υπόλοιπαToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barcodeToolStripMenuItem;
    }
}

